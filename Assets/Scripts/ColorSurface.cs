﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSurface : MonoBehaviour
{
    public Color surfaceColor;

    public Color[] colors;

    public int currentColorId;
    
    MeshRenderer renderer;

    

    [Header("Blue color")] 
    public float bumpForce;

    [Header("Orange color")] 
    public Transform behindBlock;
    public Animator blockAnimator;

    [Header("Green color")] 
    public GameObject blockPrefab;
    private GameObject currentBlock;
    
    
    void Start()
    {
        renderer = GetComponent<MeshRenderer>();
    }
    
    void Update()
    {
    }

    public void ChangeColor(int id)
    {
        currentColorId = id;
        renderer.material.color = colors[id];
    }


    
}
