﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChangeManager : MonoBehaviour
{
    private Transform cameraTransform;
    public HandColorScript handColor;

    public Color[] colors;

    public int currentColorId;
    
    void Start()
    {
        cameraTransform = Camera.main.transform;
    }

    void Update()
    {
        if (Input.GetButtonUp("LeftClic"))
        {
            RaycastHit hit;
            if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit))
            {
                if (hit.collider.tag == "ColorSurface")
                {
                    hit.collider.GetComponent<ColorSurface>().ChangeColor(currentColorId);
                }
            }
        }
        if (Input.GetButtonUp("RightClic"))
        {
            RaycastHit hit;
            if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit))
            {
                if (hit.transform.tag == "ColorSurface")
                {
                    hit.collider.GetComponent<ColorSurface>().ChangeColor(0);
                }
            }
        }

        if (Input.mouseScrollDelta.y != 0)
        {
            currentColorId += (int)Input.mouseScrollDelta.y;
            if (currentColorId >= colors.Length)
                currentColorId = 1;
            if (currentColorId <= 0)
                currentColorId = colors.Length - 1;
        }
        
        handColor.color = colors[currentColorId];
    }
}
